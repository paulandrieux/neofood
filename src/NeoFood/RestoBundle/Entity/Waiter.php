<?php

namespace NeoFood\RestoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NeoFood\RestoBundle\Entity\Waiter
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Waiter
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     * @ORM\ManyToOne(targetEntity="Restaurant", inversedBy="serveurs", cascade={"remove"})
     * @ORM\JoinColumn(name="restaurant_id", referencedColumnName="id")
     */
    protected $restaurant;
    
    /**
    * @ORM\OneToMany(targetEntity="Tble", mappedBy="waiter", cascade={"remove", "persist"})
    */
    protected $tables;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set restaurant
     *
     * @param NeoFood\RestoBundle\Entity\Restaurant $restaurant
     */
    public function setRestaurant(\NeoFood\RestoBundle\Entity\Restaurant $restaurant)
    {
        $this->restaurant = $restaurant;
    }

    /**
     * Get restaurant
     *
     * @return NeoFood\RestoBundle\Entity\Restaurant 
     */
    public function getRestaurant()
    {
        return $this->restaurant;
    }
    public function __construct()
    {
        $this->tables = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add tables
     *
     * @param NeoFood\RestoBundle\Entity\Tble $tables
     */
    public function addTble(\NeoFood\RestoBundle\Entity\Tble $tables)
    {
        $this->tables[] = $tables;
    }

    /**
     * Get tables
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getTables()
    {
        return $this->tables;
    }
}