<?php

namespace NeoFood\RestoBundle\Entity;
use FOS\UserBundle\Entity\User as BaseUser;

use Doctrine\ORM\Mapping as ORM;

/**
 * NeoFood\RestoBundle\Entity\Restaurant
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Restaurant extends BaseUser
{
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
    * @ORM\OneToMany(targetEntity="Tble", mappedBy="restaurant", cascade={"remove", "persist"})
    */
    protected $tables;
    /**
    * @ORM\OneToMany(targetEntity="Waiter", mappedBy="restaurant", cascade={"remove", "persist"})
    */
    protected $serveurs;
    

    
    
    public function __toString(){
        return $this->getUsername();
    }

    public function __construct()
    {
        parent::__construct();
//         $this->addRole(static::ROLE_RESTAURANT);
        // your own logic
    }
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add tables
     *
     * @param NeoFood\RestoBundle\Entity\Tble $tables
     */
    public function addTble(\NeoFood\RestoBundle\Entity\Tble $tables)
    {
        $this->tables[] = $tables;
    }

    /**
     * Get tables
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getTables()
    {
        return $this->tables;
    }

    /**
     * Add serveurs
     *
     * @param NeoFood\RestoBundle\Entity\Waiter $serveurs
     */
    public function addWaiter(\NeoFood\RestoBundle\Entity\Waiter $serveurs)
    {
        $this->serveurs[] = $serveurs;
    }

    /**
     * Get serveurs
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getServeurs()
    {
        return $this->serveurs;
    }

    /**
     * Add dishes
     *
     * @param NeoFood\RestoBundle\Entity\Dish $dishes
     */
    public function addDish(\NeoFood\RestoBundle\Entity\Dish $dishes)
    {
        $this->dishes[] = $dishes;
    }

    /**
     * Get dishes
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getDishes()
    {
        return $this->dishes;
    }
}