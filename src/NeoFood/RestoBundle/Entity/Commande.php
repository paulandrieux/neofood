<?php

namespace NeoFood\RestoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NeoFood\RestoBundle\Entity\Commande
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="NeoFood\RestoBundle\Entity\CommandeRepository")
 */
class Commande
{
     /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
     /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Tble")
     */
    private $table;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="NeoFood\AdminBundle\Entity\Dish")
     */
    private $dish;

    /**
     * @var string $date
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;
    
  
    
    
    const STATUS_CART = 'cart';
    const STATUS_ORDERED = 'ordered';
    const STATUS_COOKING = 'cooking';
    const STATUS_READY = 'ready';
    const STATUS_SERVED = 'served';
    const STATUS_ABORTED = 'aborted';

    /** 
     * @var string $status
     * @ORM\Column(type="string") 
     */
    protected $status;

    /**
     * Set date
     *
     * @param datetime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get date
     *
     * @return datetime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set table
     *
     * @param NeoFood\RestoBundle\Entity\Tble $table
     */
    public function setTable(\NeoFood\RestoBundle\Entity\Tble $table)
    {
        $this->table = $table;
    }

    /**
     * Get table
     *
     * @return NeoFood\RestoBundle\Entity\Tble 
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Set dish
     *
     * @param NeoFood\AdminBundle\Entity\Dish $dish
     */
    public function setDish(\NeoFood\AdminBundle\Entity\Dish $dish)
    {
        $this->dish = $dish;
    }

    /**
     * Get dish
     *
     * @return NeoFood\AdminBundle\Entity\Dish 
     */
    public function getDish()
    {
        return $this->dish;
    }

    /**
     * Set status
     *
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}