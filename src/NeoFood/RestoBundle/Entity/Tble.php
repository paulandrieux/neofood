<?php

namespace NeoFood\RestoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NeoFood\RestoBundle\Entity\Tble
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="NeoFood\RestoBundle\Entity\TbleRepository")
 */
class Tble
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
     
    
    
    /**
     * @ORM\ManyToOne(targetEntity="Restaurant", inversedBy="tables", cascade={"remove"})
     * @ORM\JoinColumn(name="restaurant_id", referencedColumnName="id")
     */
    protected $restaurant;
    
    /**
     * @ORM\ManyToOne(targetEntity="Waiter", inversedBy="tables", cascade={"remove"})
     * @ORM\JoinColumn(name="waiter_id", referencedColumnName="id")
     */
    protected $waiter;


    public function __toString(){
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    

    /**
     * Set restaurant
     *
     * @param NeoFood\RestoBundle\Entity\Restaurant $restaurant
     */
    public function setRestaurant(\NeoFood\RestoBundle\Entity\Restaurant $restaurant)
    {
        $this->restaurant = $restaurant;
    }

    /**
     * Get restaurant
     *
     * @return NeoFood\RestoBundle\Entity\Restaurant 
     */
    public function getRestaurant()
    {
        return $this->restaurant;
    }
    public function __construct()
    {
        $this->dishes = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add dishes
     *
     * @param NeoFood\AdminBundle\Entity\Dish $dishes
     */
    public function addDish(\NeoFood\AdminBundle\Entity\Dish $dishes)
    {
        $this->dishes[] = $dishes;
    }

    /**
     * Get dishes
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getDishes()
    {
        return $this->dishes;
    }

    /**
     * Set waiter
     *
     * @param NeoFood\RestoBundle\Entity\Waiter $waiter
     */
    public function setWaiter(\NeoFood\RestoBundle\Entity\Waiter $waiter)
    {
        $this->waiter = $waiter;
    }

    /**
     * Get waiter
     *
     * @return NeoFood\RestoBundle\Entity\Waiter 
     */
    public function getWaiter()
    {
        return $this->waiter;
    }
    /**
     * unset waiter
     *
     */
    public function unsetWaiter($waiter)
    {
        if($this->waiter == $waiter){
          $this->waiter = null;  
        }
        
    }
}