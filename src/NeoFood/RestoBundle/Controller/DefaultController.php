<?php

namespace NeoFood\RestoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use NeoFood\RestoBundle\Entity\Commande;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="home")
     * @Template()
     */
    public function indexAction()
    {
//        if(!isset($this->get('security.context')->getToken()->getUser())){
//            return $this->render("NeoFoodRestoBundle:Default:manage.html.twig", array('tables' => $tables->toArray(), 'serveurs' => $serveurs->toArray()));
//        }
        $session = $this->getRequest()->getSession();
        if($session->get("user") && $session->get("user_type")){
            $em = $this->getDoctrine()->getEntityManager();
            if($session->get("user_type") == "table"){
                
//                return $this->redirect($this->generateUrl("table"));
                
                $entrees = $em->getRepository('NeoFoodAdminBundle:Dish')->getActiveDishesByType('entree')->getQuery()->execute();
                $plats = $em->getRepository('NeoFoodAdminBundle:Dish')->getActiveDishesByType('plat')->getQuery()->execute();
                $desserts = $em->getRepository('NeoFoodAdminBundle:Dish')->getActiveDishesByType('dessert')->getQuery()->execute();
                $boissons = $em->getRepository('NeoFoodAdminBundle:Dish')->getActiveDishesByType('boisson')->getQuery()->execute();
                return $this->render("NeoFoodRestoBundle:Client:index.html.twig", array('entrees' => $entrees,'plats' => $plats,'desserts' => $desserts,'boissons' => $boissons ));
            
        
                
            }elseif($session->get("user_type") == "waiter"){
                
                $tables = $em->getRepository('NeoFoodRestoBundle:Tble')->getTablesByRestoByWaiter($this->get('security.context')->getToken()->getUser()->getId(), $session->get("user"))->getQuery()->execute();
                $waitings = $em->getRepository('NeoFoodRestoBundle:Commande')->getCommandesByRestoByStatus($this->get('security.context')->getToken()->getUser()->getId(), 'ordered')->getQuery()->execute();
                
                $entities = $em->getRepository('NeoFoodRestoBundle:Tble')->getTablesByResto($this->get('security.context')->getToken()->getUser()->getId())->getQuery()->execute();
                return $this->render("NeoFoodRestoBundle:Waiter:select-table.html.twig", array('tables' => $entities, 'currentUser' => $session->get("user")));
                
            }elseif($session->get("user_type") == "cooker"){
                
                $tables = $em->getRepository('NeoFoodRestoBundle:Tble')->getTablesByResto($this->get('security.context')->getToken()->getUser()->getId())->getQuery()->execute();
                $waitings = $em->getRepository('NeoFoodRestoBundle:Commande')->getCommandesByRestoByStatus($this->get('security.context')->getToken()->getUser()->getId(), 'ordered')->getQuery()->execute();
                $cookings = $em->getRepository('NeoFoodRestoBundle:Commande')->getCommandesByRestoByStatus($this->get('security.context')->getToken()->getUser()->getId(), 'cooking')->getQuery()->execute();
                return $this->render("NeoFoodRestoBundle:Default:kitchen.html.twig", array('waitings' => $waitings, 'cookings' => $cookings));
            }
        }else{
            $user = $this->get('security.context')->getToken()->getUser();
            $tables = $user->getTables();
            $serveurs = $user->getServeurs();
//            return array('tables' => $tables->toArray(), 'serveurs' => $serveurs->toArray());
            return $this->render("NeoFoodRestoBundle:Default:manage.html.twig", array('tables' => $tables->toArray(), 'serveurs' => $serveurs->toArray()));
            
//            return $this->redirect($this->generateUrl("manage"));
        }
    }
    
    /**
     * @Route("/serveur", name="watchTables")
     * @Template()
     */
    public function watchTablesAction()
    {
         $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getEntityManager();
        $tables = $em->getRepository('NeoFoodRestoBundle:Tble')->getTablesByRestoByWaiter($this->get('security.context')->getToken()->getUser()->getId(), $session->get("user"))->getQuery()->execute();
        $waitings = $em->getRepository('NeoFoodRestoBundle:Commande')->getCommandesByWaiterByStatus($session->get("user"), 'ordered')->getQuery()->execute();

        $entities = $em->getRepository('NeoFoodRestoBundle:Tble')->getTablesByResto($this->get('security.context')->getToken()->getUser()->getId())->getQuery()->execute();
        return $this->render("NeoFoodRestoBundle:Waiter:watch-tables.html.twig", array('tables' => $entities, 'commandes' => $waitings));
                
    }
    /**
     * @Route("/table", name="table")
     * @Template()
     */
    public function tableAction()
    {
        
        $em = $this->getDoctrine()->getEntityManager();
        $entrees = $em->getRepository('NeoFoodAdminBundle:Dish')->getActiveDishesByType('entree')->getQuery()->execute();
        $plats = $em->getRepository('NeoFoodAdminBundle:Dish')->getActiveDishesByType('plat')->getQuery()->execute();
        $desserts = $em->getRepository('NeoFoodAdminBundle:Dish')->getActiveDishesByType('dessert')->getQuery()->execute();
        $boissons = $em->getRepository('NeoFoodAdminBundle:Dish')->getActiveDishesByType('boisson')->getQuery()->execute();
        return $this->render("NeoFoodRestoBundle:Client:index.html.twig", array('entrees' => $entrees,'plats' => $plats,'desserts' => $desserts,'boissons' => $boissons ));
            
    }
    
    /**
     * @Route("/detailDish", name="viewDetail")
     * @Template()
     */
    public function viewDetailsAction()
    {
        $request = $this->container->get('request');
         if ($request->isXmlHttpRequest()) {
            $dishId = $request->request->get('dishId');
        $em = $this->getDoctrine()->getEntityManager();
        $dish = $em->getRepository('NeoFoodAdminBundle:Dish')->findOneById($dishId);
        $dishDetails = array(
            'title' => $dish->getTitle(),
            'price' => $dish->getPrice(),
            'description' => $dish->getDescription(),
            'id' => $dish->getId(),
                );
        $return=json_encode($dishDetails);
        $response = new Response($return,200,array('Content-Type'=>'application/json'));
        return $response;
        }
        
    }
    
    
    /**
     * @Route("/ajouter-au-panier", name="addToCart")
     * @Template()
     */
    public function addToCartAction()
    {
        $request = $this->container->get('request');
         if ($request->isXmlHttpRequest()) {
            $dishId = $request->request->get('dishId');
            $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getEntityManager();
        $table = $em->getRepository('NeoFoodRestoBundle:Tble')->findOneById($session->get("user"));
        $dish = $em->getRepository('NeoFoodAdminBundle:Dish')->findOneById($dishId);
//        $dish = $this->getRequest()->get("dish");
        $commande = new Commande();
        $commande->setTable($table);
        $commande->setDish($dish);
        $commande->setStatus(Commande::STATUS_CART);
//        print_r(time());exit;
        $date = new \DateTime();
        $commande->setDate($date);
        $em->persist($commande);
        $metadata = $em->getClassMetaData(get_class($commande));
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);

        $em->flush();
        $return=json_encode('ok');
        $response = new Response($return,200,array('Content-Type'=>'application/json'));
        return $response;
        }
        
    }
    /**
     * @Route("/retirer-du-panier", name="subToCart")
     * @Template()
     */
    public function subToCartAction()
    {
        $request = $this->container->get('request');
         if ($request->isXmlHttpRequest()) {
            $commandeId = $request->request->get('commandeId');
            $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getEntityManager();
        $commande = $em->getRepository('NeoFoodRestoBundle:Commande')->findOneById($commandeId);
        $commande->setStatus(Commande::STATUS_ABORTED);
        $em->persist($commande);
        
        $em->flush();
        $return=json_encode('ok');
        $response = new Response($return,200,array('Content-Type'=>'application/json'));
        return $response;
        }
        
    }
    /**
     * @Route("/visualiser-panier", name="showCart")
     * @Template("NeoFoodRestoBundle:Client:cart.html.twig")
     */
    public function showCartAction()
    {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getEntityManager();
        $commandes = $em->getRepository('NeoFoodRestoBundle:Commande')->getCartByTableId($session->get("user"))->getQuery()->execute();
        $dishes = null;
        $price = 0;
        foreach($commandes as $k => $commande){
            $dishes[$k] = $commande;
            $price = $price+$commande->getDish()->getPrice();
        }
        return array('entities' => $dishes, 'totalPrice' => $price);
    }
    
    /**
     * @Route("/paiement", name="submitCart")
     * @Template("NeoFoodRestoBundle:Client:paiement.html.twig")
     */
    public function submitCartAction()
    {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getEntityManager();
        $commandes = $em->getRepository('NeoFoodRestoBundle:Commande')->getCartByTableId($session->get("user"))->getQuery()->execute();
        
        $price = 0;
        foreach($commandes as $k => $commande){
            $price = $price+$commande->getDish()->getPrice();
        }
        return array('totalPrice' => $price);
    }
    
    /**
     * @Route("/paiement-valide", name="validPaiementCart")
     * @Template("NeoFoodRestoBundle:Client:valid-paiement.html.twig")
     */
    public function validPaiementCartAction()
    {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getEntityManager();
        $commandes = $em->getRepository('NeoFoodRestoBundle:Commande')->getCartByTableId($session->get("user"))->getQuery()->execute();
        foreach($commandes as $k => $commande){
        $commande->setStatus(Commande::STATUS_ORDERED);
        $em->persist($commande);
        }
//        $metadata = $em->getClassMetaData(get_class($commande));
//        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);

        $em->flush();
        return array();
        
        
    }
    
     /**
     * @Route("/preparer-un-plat/{commandeId}", name="cook")
     * @Template()
     */
    public function cookAction($commandeId)
    {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getEntityManager();
        $commande = $em->getRepository('NeoFoodRestoBundle:Commande')->findOneById($commandeId);
        $commande->setStatus(Commande::STATUS_COOKING);
        $em->persist($commande);
//        $metadata = $em->getClassMetaData(get_class($commande));
//        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);

        $em->flush();
        return $this->redirect($this->generateUrl("home"));
    }
    
     /**
     * @Route("/plat-pret/{commandeId}", name="cookReady")
     * @Template()
     */
    public function cookReadyAction($commandeId)
    {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getEntityManager();
        $commande = $em->getRepository('NeoFoodRestoBundle:Commande')->findOneById($commandeId);
        $commande->setStatus(Commande::STATUS_READY);
        $em->persist($commande);
//        $metadata = $em->getClassMetaData(get_class($commande));
//        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);

        $em->flush();
        return $this->redirect($this->generateUrl("home"));
    }
    
    
    /**
     * @Route("/manage", name="manage")
     * @Template()
     */
    public function manageAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $tables = $user->getTables();
        $serveurs = $user->getServeurs();
        return array('tables' => $tables->toArray(), 'serveurs' => $serveurs->toArray());
    }
    
    
    /**
     * @Route("/assignWaiter", name="assignWaiter")
     * @Method("post")
     * @Template()
     */
    public function assignWaiterAction()
    {
        $resto = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();
        $session = $this->getRequest()->getSession();
        $waiter = $em->getRepository('NeoFoodRestoBundle:Waiter')->findOneById($session->get("user"));
        $checked = $this->getRequest()->get("checked");
        $tables = $em->getRepository('NeoFoodRestoBundle:Tble')->getTablesByResto($resto->getId())->getQuery()->execute();
        foreach ($tables as $table) {
            $checked = $this->getRequest()->get("checkbox-".$table->getId());
        
            if ($checked == true) {
                $table->setWaiter($waiter);
            }else{
                $table->unsetWaiter($waiter);
            }
        }
        $em->persist($table);

        $em->flush();
        
        return $this->redirect($this->generateUrl("home"));
    }
    
    
    /**
     * 
     * @Route("/user/{type}/{id}", name="user")
     * @Template()
     */
    public function userAction($type, $id)
    {
        $user = $id;
        $userType = $type;
        $session = $this->getRequest()->getSession();
        $session->set('user' ,$user);
        $session->set('user_type' ,$userType);
        return $this->redirect($this->generateUrl("home"));
//        return $this->indexAction();
    }
    
    /**
     * 
     * @Route("/reset", name="reset")
     * @Template()
     */
    public function resetAction()
    {
        $this->getRequest()->getSession()->clear();
        return $this->redirect($this->generateUrl("home"));
    }
    
    
}
