<?php

namespace NeoFood\RestoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use NeoFood\RestoBundle\Entity\Waiter;
use NeoFood\RestoBundle\Form\WaiterType;

/**
 * Waiter controller.
 *
 * @Route("/waiter")
 */
class WaiterController extends Controller
{
    /**
     * Lists all Waiter entities.
     *
     * @Route("/", name="waiter")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('NeoFoodRestoBundle:Waiter')->findAll();

        return array('entities' => $entities);
    }

    /**
     * Finds and displays a Waiter entity.
     *
     * @Route("/{id}/show", name="waiter_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('NeoFoodRestoBundle:Waiter')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Waiter entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        );
    }

    /**
     * Displays a form to create a new Waiter entity.
     *
     * @Route("/new", name="waiter_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Waiter();
        $form   = $this->createForm(new WaiterType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Creates a new Waiter entity.
     *
     * @Route("/create", name="waiter_create")
     * @Method("post")
     * @Template("NeoFoodRestoBundle:Waiter:new.html.twig")
     */
    public function createAction()
    {
        $entity  = new Waiter();
        $restaurant = $this->get('security.context')->getToken()->getUser();
        $entity->setRestaurant($restaurant);
        $request = $this->getRequest();
        $form    = $this->createForm(new WaiterType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('home'));
            
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Waiter entity.
     *
     * @Route("/{id}/edit", name="waiter_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('NeoFoodRestoBundle:Waiter')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Waiter entity.');
        }

        $editForm = $this->createForm(new WaiterType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Waiter entity.
     *
     * @Route("/{id}/update", name="waiter_update")
     * @Method("post")
     * @Template("NeoFoodRestoBundle:Waiter:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('NeoFoodRestoBundle:Waiter')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Waiter entity.');
        }

        $editForm   = $this->createForm(new WaiterType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('waiter_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Waiter entity.
     *
     * @Route("/{id}/delete", name="waiter_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('NeoFoodRestoBundle:Waiter')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Waiter entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('waiter'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
