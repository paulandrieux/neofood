<?php

namespace NeoFood\RestoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use NeoFood\RestoBundle\Entity\Tble;
use NeoFood\RestoBundle\Form\TbleType;

/**
 * Tble controller.
 *
 * @Route("/tble")
 */
class TbleController extends Controller
{
    /**
     * Lists all Tble entities.
     *
     * @Route("/", name="tble")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

//        $entities = $em->getRepository('NeoFoodRestoBundle:Tble')->findAll();
        $entities = $em->getRepository('NeoFoodRestoBundle:Tble')->findByRestaurant($this->get('security.context')->getToken()->getUser()->getId());

        return array('entities' => $entities);
    }

    /**
     * Finds and displays a Tble entity.
     *
     * @Route("/{id}/show", name="tble_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('NeoFoodRestoBundle:Tble')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tble entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        );
    }

    /**
     * Displays a form to create a new Tble entity.
     *
     * @Route("/new", name="tble_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Tble();
        $form   = $this->createForm(new TbleType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Creates a new Tble entity.
     *
     * @Route("/create", name="tble_create")
     * @Method("post")
     * @Template("NeoFoodRestoBundle:Tble:new.html.twig")
     */
    public function createAction()
    {
        $entity  = new Tble();
        $restaurant = $this->get('security.context')->getToken()->getUser();
        $entity->setRestaurant($restaurant);
        $request = $this->getRequest();
        $form    = $this->createForm(new TbleType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('home'));
            
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Tble entity.
     *
     * @Route("/{id}/edit", name="tble_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('NeoFoodRestoBundle:Tble')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tble entity.');
        }

        $editForm = $this->createForm(new TbleType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Tble entity.
     *
     * @Route("/{id}/update", name="tble_update")
     * @Method("post")
     * @Template("NeoFoodRestoBundle:Tble:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('NeoFoodRestoBundle:Tble')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tble entity.');
        }

        $editForm   = $this->createForm(new TbleType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tble_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Tble entity.
     *
     * @Route("/{id}/delete", name="tble_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('NeoFoodRestoBundle:Tble')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Tble entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tble'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
