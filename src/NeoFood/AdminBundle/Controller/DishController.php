<?php

namespace NeoFood\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use NeoFood\AdminBundle\Entity\Dish;
use NeoFood\AdminBundle\Form\DishType;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Dish controller.
 *
 * @Route("/dish")
 */
class DishController extends Controller
{
    
    

    /**
     * Finds and displays a Dish entity.
     *
     * @Route("/{id}/show", name="dish_show")
     * @Template()
     */
    public function showAction($id)
    {
        if( ! $this->get('security.context')->isGranted('ROLE_ADMIN') )
        {
            // Sinon on déclenche une exception "Accès Interdit"
            throw new AccessDeniedHttpException("Accès limité a l'administrateur");
        }
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('NeoFoodAdminBundle:Dish')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Dish entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        );
    }

    /**
     * Displays a form to create a new Dish entity.
     *
     * @Route("/new", name="dish_new")
     * @Template()
     */
    public function newAction()
    {
        if( ! $this->get('security.context')->isGranted('ROLE_ADMIN') )
        {
            // Sinon on déclenche une exception "Accès Interdit"
            throw new AccessDeniedHttpException("Accès limité a l'administrateur");
        }
        $entity = new Dish();
        $form   = $this->createForm(new DishType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Creates a new Dish entity.
     *
     * @Route("/create", name="dish_create")
     * @Method("post")
     * @Template("NeoFoodAdminBundle:Dish:new.html.twig")
     */
    public function createAction()
    {
        if( ! $this->get('security.context')->isGranted('ROLE_ADMIN') )
        {
            // Sinon on déclenche une exception "Accès Interdit"
            throw new AccessDeniedHttpException("Accès limité a l'administrateur");
        }
        $entity  = new Dish();
        $request = $this->getRequest();
        $form    = $this->createForm(new DishType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('dish'));
            
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Dish entity.
     *
     * @Route("/{id}/edit", name="dish_edit")
     * @Template()
     */
    public function editAction($id)
    {
        if( ! $this->get('security.context')->isGranted('ROLE_ADMIN') )
        {
            // Sinon on déclenche une exception "Accès Interdit"
            throw new AccessDeniedHttpException("Accès limité a l'administrateur");
        }
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('NeoFoodAdminBundle:Dish')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Dish entity.');
        }

        $editForm = $this->createForm(new DishType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Dish entity.
     *
     * @Route("/{id}/update", name="dish_update")
     * @Method("post")
     * @Template("NeoFoodAdminBundle:Dish:edit.html.twig")
     */
    public function updateAction($id)
    {
        if( ! $this->get('security.context')->isGranted('ROLE_ADMIN') )
        {
            // Sinon on déclenche une exception "Accès Interdit"
            throw new AccessDeniedHttpException("Accès limité a l'administrateur");
        }
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('NeoFoodAdminBundle:Dish')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Dish entity.');
        }

        $editForm   = $this->createForm(new DishType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('dish_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Dish entity.
     *
     * @Route("/{id}/delete", name="dish_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        if( ! $this->get('security.context')->isGranted('ROLE_ADMIN') )
        {
            // Sinon on déclenche une exception "Accès Interdit"
            throw new AccessDeniedHttpException("Accès limité a l'administrateur");
        }
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('NeoFoodAdminBundle:Dish')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Dish entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('dish'));
    }

    private function createDeleteForm($id)
    {
        if( ! $this->get('security.context')->isGranted('ROLE_ADMIN') )
        {
            // Sinon on déclenche une exception "Accès Interdit"
            throw new AccessDeniedHttpException("Accès limité a l'administrateur");
        }
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
