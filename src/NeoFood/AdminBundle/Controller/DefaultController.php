<?php

namespace NeoFood\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use NeoFood\AdminBundle\Entity\Dish;
use NeoFood\AdminBundle\Form\DishType;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="admin")
     * @Template()
     */
    public function indexAction()
    {
      if( ! $this->get('security.context')->isGranted('ROLE_ADMIN') )
        {
            // Sinon on déclenche une exception "Accès Interdit"
            throw new AccessDeniedHttpException("Accès limité a l'administrateur");
        }
            $em = $this->getDoctrine()->getEntityManager();
            $entities = $em->getRepository('NeoFoodAdminBundle:Dish')->findAll();
            return $this->render("NeoFoodAdminBundle:Default:index.html.twig", array('entities' => $entities));
    }
    /**
     * @Route("/liste-des-plats", name="dish")
     * @Template()
     */
    public function listDishesAction()
    {
      if( ! $this->get('security.context')->isGranted('ROLE_ADMIN') )
        {
            // Sinon on déclenche une exception "Accès Interdit"
            throw new AccessDeniedHttpException("Accès limité a l'administrateur");
        }
            $em = $this->getDoctrine()->getEntityManager();
            $entities = $em->getRepository('NeoFoodAdminBundle:Dish')->findAll();
            return $this->render("NeoFoodAdminBundle:Dish:index.html.twig", array('entities' => $entities));
    }
    /**
     * Finds and displays a Dish entity.
     *
     * @Route("/{id}/show", name="dish_show")
     * @Template()
     */
    public function showAction($id)
    {
        if( ! $this->get('security.context')->isGranted('ROLE_ADMIN') )
        {
            // Sinon on déclenche une exception "Accès Interdit"
            throw new AccessDeniedHttpException("Accès limité a l'administrateur");
        }
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('NeoFoodAdminBundle:Dish')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Dish entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        );
    }
    
    
    
    
    
}
