<?php

namespace NeoFood\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use NeoFood\AdminBundle\Entity;

class DishType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        
        $builder
            ->add('title')
            ->add('price')
            ->add('type', 'choice', array(
                'choices'   => array(
                    Entity\Dish::TYPE_ENTREE => 'entree', 
                    Entity\Dish::TYPE_PLAT => 'plat', 
                    Entity\Dish::TYPE_DESSERT => 'dessert', 
                    Entity\Dish::TYPE_BOISSON => 'boisson', 
                    ),
                'required'  => true,
            ))
            ->add('description', 'textarea', array('required' => false))
            ->add('active', 'checkbox', array('required' => false))
        ;
    }

    public function getName()
    {
        return 'neofood_adminbundle_dishtype';
    }
}
