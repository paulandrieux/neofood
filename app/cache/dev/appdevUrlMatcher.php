<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appdevUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appdevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = urldecode($pathinfo);

        // _demo_login
        if ($pathinfo === '/demo/secured/login') {
            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::loginAction',  '_route' => '_demo_login',);
        }

        // _security_check
        if ($pathinfo === '/demo/secured/login_check') {
            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::securityCheckAction',  '_route' => '_security_check',);
        }

        // _demo_logout
        if ($pathinfo === '/demo/secured/logout') {
            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::logoutAction',  '_route' => '_demo_logout',);
        }

        // acme_demo_secured_hello
        if ($pathinfo === '/demo/secured/hello') {
            return array (  'name' => 'World',  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',  '_route' => 'acme_demo_secured_hello',);
        }

        // _demo_secured_hello
        if (0 === strpos($pathinfo, '/demo/secured/hello') && preg_match('#^/demo/secured/hello/(?P<name>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',)), array('_route' => '_demo_secured_hello'));
        }

        // _demo_secured_hello_admin
        if (0 === strpos($pathinfo, '/demo/secured/hello/admin') && preg_match('#^/demo/secured/hello/admin/(?P<name>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloadminAction',)), array('_route' => '_demo_secured_hello_admin'));
        }

        if (0 === strpos($pathinfo, '/demo')) {
            // _demo
            if (rtrim($pathinfo, '/') === '/demo') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_demo');
                }
                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::indexAction',  '_route' => '_demo',);
            }

            // _demo_hello
            if (0 === strpos($pathinfo, '/demo/hello') && preg_match('#^/demo/hello/(?P<name>[^/]+?)$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::helloAction',)), array('_route' => '_demo_hello'));
            }

            // _demo_contact
            if ($pathinfo === '/demo/contact') {
                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::contactAction',  '_route' => '_demo_contact',);
            }

        }

        // _wdt
        if (preg_match('#^/_wdt/(?P<token>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::toolbarAction',)), array('_route' => '_wdt'));
        }

        if (0 === strpos($pathinfo, '/_profiler')) {
            // _profiler_search
            if ($pathinfo === '/_profiler/search') {
                return array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchAction',  '_route' => '_profiler_search',);
            }

            // _profiler_purge
            if ($pathinfo === '/_profiler/purge') {
                return array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::purgeAction',  '_route' => '_profiler_purge',);
            }

            // _profiler_import
            if ($pathinfo === '/_profiler/import') {
                return array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::importAction',  '_route' => '_profiler_import',);
            }

            // _profiler_export
            if (0 === strpos($pathinfo, '/_profiler/export') && preg_match('#^/_profiler/export/(?P<token>[^/\\.]+?)\\.txt$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::exportAction',)), array('_route' => '_profiler_export'));
            }

            // _profiler_search_results
            if (preg_match('#^/_profiler/(?P<token>[^/]+?)/search/results$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchResultsAction',)), array('_route' => '_profiler_search_results'));
            }

            // _profiler
            if (preg_match('#^/_profiler/(?P<token>[^/]+?)$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::panelAction',)), array('_route' => '_profiler'));
            }

        }

        if (0 === strpos($pathinfo, '/_configurator')) {
            // _configurator_home
            if (rtrim($pathinfo, '/') === '/_configurator') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_configurator_home');
                }
                return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
            }

            // _configurator_step
            if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]+?)$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',)), array('_route' => '_configurator_step'));
            }

            // _configurator_final
            if ($pathinfo === '/_configurator/final') {
                return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
            }

        }

        // neofood_twitterboostrap_default_index
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'NeoFood\\TwitterBoostrapBundle\\Controller\\DefaultController::indexAction',)), array('_route' => 'neofood_twitterboostrap_default_index'));
        }

        if (0 === strpos($pathinfo, '/administrer')) {
            // dish_new
            if ($pathinfo === '/administrer/dish/new') {
                return array (  '_controller' => 'NeoFood\\AdminBundle\\Controller\\DishController::newAction',  '_route' => 'dish_new',);
            }

            // dish_create
            if ($pathinfo === '/administrer/dish/create') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_dish_create;
                }
                return array (  '_controller' => 'NeoFood\\AdminBundle\\Controller\\DishController::createAction',  '_route' => 'dish_create',);
            }
            not_dish_create:

            // dish_edit
            if (0 === strpos($pathinfo, '/administrer/dish') && preg_match('#^/administrer/dish/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'NeoFood\\AdminBundle\\Controller\\DishController::editAction',)), array('_route' => 'dish_edit'));
            }

            // dish_update
            if (0 === strpos($pathinfo, '/administrer/dish') && preg_match('#^/administrer/dish/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_dish_update;
                }
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'NeoFood\\AdminBundle\\Controller\\DishController::updateAction',)), array('_route' => 'dish_update'));
            }
            not_dish_update:

            // dish_delete
            if (0 === strpos($pathinfo, '/administrer/dish') && preg_match('#^/administrer/dish/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_dish_delete;
                }
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'NeoFood\\AdminBundle\\Controller\\DishController::deleteAction',)), array('_route' => 'dish_delete'));
            }
            not_dish_delete:

            // admin
            if (rtrim($pathinfo, '/') === '/administrer') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'admin');
                }
                return array (  '_controller' => 'NeoFood\\AdminBundle\\Controller\\DefaultController::indexAction',  '_route' => 'admin',);
            }

            // dish
            if ($pathinfo === '/administrer/liste-des-plats') {
                return array (  '_controller' => 'NeoFood\\AdminBundle\\Controller\\DefaultController::listDishesAction',  '_route' => 'dish',);
            }

            // dish_show
            if (preg_match('#^/administrer/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'NeoFood\\AdminBundle\\Controller\\DefaultController::showAction',)), array('_route' => 'dish_show'));
            }

        }

        // tble
        if (rtrim($pathinfo, '/') === '/tble') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'tble');
            }
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\TbleController::indexAction',  '_route' => 'tble',);
        }

        // tble_show
        if (0 === strpos($pathinfo, '/tble') && preg_match('#^/tble/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\TbleController::showAction',)), array('_route' => 'tble_show'));
        }

        // tble_new
        if ($pathinfo === '/tble/new') {
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\TbleController::newAction',  '_route' => 'tble_new',);
        }

        // tble_create
        if ($pathinfo === '/tble/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_tble_create;
            }
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\TbleController::createAction',  '_route' => 'tble_create',);
        }
        not_tble_create:

        // tble_edit
        if (0 === strpos($pathinfo, '/tble') && preg_match('#^/tble/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\TbleController::editAction',)), array('_route' => 'tble_edit'));
        }

        // tble_update
        if (0 === strpos($pathinfo, '/tble') && preg_match('#^/tble/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_tble_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\TbleController::updateAction',)), array('_route' => 'tble_update'));
        }
        not_tble_update:

        // tble_delete
        if (0 === strpos($pathinfo, '/tble') && preg_match('#^/tble/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_tble_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\TbleController::deleteAction',)), array('_route' => 'tble_delete'));
        }
        not_tble_delete:

        // restaurant
        if (rtrim($pathinfo, '/') === '/restaurant') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'restaurant');
            }
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\RestaurantController::indexAction',  '_route' => 'restaurant',);
        }

        // restaurant_show
        if (0 === strpos($pathinfo, '/restaurant') && preg_match('#^/restaurant/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\RestaurantController::showAction',)), array('_route' => 'restaurant_show'));
        }

        // restaurant_new
        if ($pathinfo === '/restaurant/new') {
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\RestaurantController::newAction',  '_route' => 'restaurant_new',);
        }

        // restaurant_create
        if ($pathinfo === '/restaurant/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_restaurant_create;
            }
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\RestaurantController::createAction',  '_route' => 'restaurant_create',);
        }
        not_restaurant_create:

        // restaurant_edit
        if (0 === strpos($pathinfo, '/restaurant') && preg_match('#^/restaurant/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\RestaurantController::editAction',)), array('_route' => 'restaurant_edit'));
        }

        // restaurant_update
        if (0 === strpos($pathinfo, '/restaurant') && preg_match('#^/restaurant/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_restaurant_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\RestaurantController::updateAction',)), array('_route' => 'restaurant_update'));
        }
        not_restaurant_update:

        // restaurant_delete
        if (0 === strpos($pathinfo, '/restaurant') && preg_match('#^/restaurant/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_restaurant_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\RestaurantController::deleteAction',)), array('_route' => 'restaurant_delete'));
        }
        not_restaurant_delete:

        // fos_user_registration_register_custom
        if ($pathinfo === '/register') {
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\RestoRegistrationController::registerAction',  '_route' => 'fos_user_registration_register_custom',);
        }

        // home
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'home');
            }
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::indexAction',  '_route' => 'home',);
        }

        // watchTables
        if ($pathinfo === '/serveur') {
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::watchTablesAction',  '_route' => 'watchTables',);
        }

        // table
        if ($pathinfo === '/table') {
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::tableAction',  '_route' => 'table',);
        }

        // viewDetail
        if ($pathinfo === '/detailDish') {
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::viewDetailsAction',  '_route' => 'viewDetail',);
        }

        // addToCart
        if ($pathinfo === '/ajouter-au-panier') {
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::addToCartAction',  '_route' => 'addToCart',);
        }

        // subToCart
        if ($pathinfo === '/retirer-du-panier') {
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::subToCartAction',  '_route' => 'subToCart',);
        }

        // showCart
        if ($pathinfo === '/visualiser-panier') {
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::showCartAction',  '_route' => 'showCart',);
        }

        // submitCart
        if ($pathinfo === '/paiement') {
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::submitCartAction',  '_route' => 'submitCart',);
        }

        // validPaiementCart
        if ($pathinfo === '/paiement-valide') {
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::validPaiementCartAction',  '_route' => 'validPaiementCart',);
        }

        // cook
        if (0 === strpos($pathinfo, '/preparer-un-plat') && preg_match('#^/preparer\\-un\\-plat/(?P<commandeId>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::cookAction',)), array('_route' => 'cook'));
        }

        // cookReady
        if (0 === strpos($pathinfo, '/plat-pret') && preg_match('#^/plat\\-pret/(?P<commandeId>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::cookReadyAction',)), array('_route' => 'cookReady'));
        }

        // manage
        if ($pathinfo === '/manage') {
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::manageAction',  '_route' => 'manage',);
        }

        // assignWaiter
        if ($pathinfo === '/assignWaiter') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_assignWaiter;
            }
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::assignWaiterAction',  '_route' => 'assignWaiter',);
        }
        not_assignWaiter:

        // user
        if (0 === strpos($pathinfo, '/user') && preg_match('#^/user/(?P<type>[^/]+?)/(?P<id>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::userAction',)), array('_route' => 'user'));
        }

        // reset
        if ($pathinfo === '/reset') {
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::resetAction',  '_route' => 'reset',);
        }

        // waiter
        if (rtrim($pathinfo, '/') === '/waiter') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'waiter');
            }
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\WaiterController::indexAction',  '_route' => 'waiter',);
        }

        // waiter_show
        if (0 === strpos($pathinfo, '/waiter') && preg_match('#^/waiter/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\WaiterController::showAction',)), array('_route' => 'waiter_show'));
        }

        // waiter_new
        if ($pathinfo === '/waiter/new') {
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\WaiterController::newAction',  '_route' => 'waiter_new',);
        }

        // waiter_create
        if ($pathinfo === '/waiter/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_waiter_create;
            }
            return array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\WaiterController::createAction',  '_route' => 'waiter_create',);
        }
        not_waiter_create:

        // waiter_edit
        if (0 === strpos($pathinfo, '/waiter') && preg_match('#^/waiter/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\WaiterController::editAction',)), array('_route' => 'waiter_edit'));
        }

        // waiter_update
        if (0 === strpos($pathinfo, '/waiter') && preg_match('#^/waiter/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_waiter_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\WaiterController::updateAction',)), array('_route' => 'waiter_update'));
        }
        not_waiter_update:

        // waiter_delete
        if (0 === strpos($pathinfo, '/waiter') && preg_match('#^/waiter/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_waiter_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\WaiterController::deleteAction',)), array('_route' => 'waiter_delete'));
        }
        not_waiter_delete:

        // fos_user_security_login
        if ($pathinfo === '/login') {
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
        }

        // fos_user_security_check
        if ($pathinfo === '/login_check') {
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
        }

        // fos_user_security_logout
        if ($pathinfo === '/logout') {
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ($pathinfo === '/profile/edit') {
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
            }

            // fos_user_change_password
            if ($pathinfo === '/profile/change-password') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_change_password;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
            }
            not_fos_user_change_password:

        }

        if (0 === strpos($pathinfo, '/register')) {
            // fos_user_registration_register
            if (rtrim($pathinfo, '/') === '/register') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
            }

            // fos_user_registration_check_email
            if ($pathinfo === '/register/check-email') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_registration_check_email;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
            }
            not_fos_user_registration_check_email:

            // fos_user_registration_confirm
            if (0 === strpos($pathinfo, '/register/confirm') && preg_match('#^/register/confirm/(?P<token>[^/]+?)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_registration_confirm;
                }
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',)), array('_route' => 'fos_user_registration_confirm'));
            }
            not_fos_user_registration_confirm:

            // fos_user_registration_confirmed
            if ($pathinfo === '/register/confirmed') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_registration_confirmed;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
            }
            not_fos_user_registration_confirmed:

        }

        if (0 === strpos($pathinfo, '/resetting')) {
            // fos_user_resetting_request
            if ($pathinfo === '/resetting/request') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_resetting_request;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
            }
            not_fos_user_resetting_request:

            // fos_user_resetting_send_email
            if ($pathinfo === '/resetting/send-email') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_fos_user_resetting_send_email;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
            }
            not_fos_user_resetting_send_email:

            // fos_user_resetting_check_email
            if ($pathinfo === '/resetting/check-email') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_resetting_check_email;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
            }
            not_fos_user_resetting_check_email:

            // fos_user_resetting_reset
            if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]+?)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_resetting_reset;
                }
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',)), array('_route' => 'fos_user_resetting_reset'));
            }
            not_fos_user_resetting_reset:

        }

        // orderly_paypalipn_twignotificationemail_index
        if ($pathinfo === '/ipn/ipn-twig-email-notification') {
            return array (  '_controller' => 'Orderly\\PayPalIpnBundle\\Controller\\TwigNotificationEmailController::indexAction',  '_route' => 'orderly_paypalipn_twignotificationemail_index',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
