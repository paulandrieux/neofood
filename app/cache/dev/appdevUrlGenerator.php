<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;


/**
 * appdevUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appdevUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    static private $declaredRouteNames = array(
       '_demo_login' => true,
       '_security_check' => true,
       '_demo_logout' => true,
       'acme_demo_secured_hello' => true,
       '_demo_secured_hello' => true,
       '_demo_secured_hello_admin' => true,
       '_demo' => true,
       '_demo_hello' => true,
       '_demo_contact' => true,
       '_wdt' => true,
       '_profiler_search' => true,
       '_profiler_purge' => true,
       '_profiler_import' => true,
       '_profiler_export' => true,
       '_profiler_search_results' => true,
       '_profiler' => true,
       '_configurator_home' => true,
       '_configurator_step' => true,
       '_configurator_final' => true,
       'neofood_twitterboostrap_default_index' => true,
       'dish_new' => true,
       'dish_create' => true,
       'dish_edit' => true,
       'dish_update' => true,
       'dish_delete' => true,
       'admin' => true,
       'dish' => true,
       'dish_show' => true,
       'tble' => true,
       'tble_show' => true,
       'tble_new' => true,
       'tble_create' => true,
       'tble_edit' => true,
       'tble_update' => true,
       'tble_delete' => true,
       'restaurant' => true,
       'restaurant_show' => true,
       'restaurant_new' => true,
       'restaurant_create' => true,
       'restaurant_edit' => true,
       'restaurant_update' => true,
       'restaurant_delete' => true,
       'fos_user_registration_register_custom' => true,
       'home' => true,
       'watchTables' => true,
       'table' => true,
       'viewDetail' => true,
       'addToCart' => true,
       'subToCart' => true,
       'showCart' => true,
       'submitCart' => true,
       'validPaiementCart' => true,
       'cook' => true,
       'cookReady' => true,
       'manage' => true,
       'assignWaiter' => true,
       'user' => true,
       'reset' => true,
       'waiter' => true,
       'waiter_show' => true,
       'waiter_new' => true,
       'waiter_create' => true,
       'waiter_edit' => true,
       'waiter_update' => true,
       'waiter_delete' => true,
       'fos_user_security_login' => true,
       'fos_user_security_check' => true,
       'fos_user_security_logout' => true,
       'fos_user_profile_show' => true,
       'fos_user_profile_edit' => true,
       'fos_user_registration_register' => true,
       'fos_user_registration_check_email' => true,
       'fos_user_registration_confirm' => true,
       'fos_user_registration_confirmed' => true,
       'fos_user_resetting_request' => true,
       'fos_user_resetting_send_email' => true,
       'fos_user_resetting_check_email' => true,
       'fos_user_resetting_reset' => true,
       'fos_user_change_password' => true,
       'orderly_paypalipn_twignotificationemail_index' => true,
    );

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function generate($name, $parameters = array(), $absolute = false)
    {
        if (!isset(self::$declaredRouteNames[$name])) {
            throw new RouteNotFoundException(sprintf('Route "%s" does not exist.', $name));
        }

        $escapedName = str_replace('.', '__', $name);

        list($variables, $defaults, $requirements, $tokens) = $this->{'get'.$escapedName.'RouteInfo'}();

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $absolute);
    }

    private function get_demo_loginRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::loginAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/demo/secured/login',  ),));
    }

    private function get_security_checkRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::securityCheckAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/demo/secured/login_check',  ),));
    }

    private function get_demo_logoutRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::logoutAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/demo/secured/logout',  ),));
    }

    private function getacme_demo_secured_helloRouteInfo()
    {
        return array(array (), array (  'name' => 'World',  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/demo/secured/hello',  ),));
    }

    private function get_demo_secured_helloRouteInfo()
    {
        return array(array (  0 => 'name',), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'name',  ),  1 =>   array (    0 => 'text',    1 => '/demo/secured/hello',  ),));
    }

    private function get_demo_secured_hello_adminRouteInfo()
    {
        return array(array (  0 => 'name',), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloadminAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'name',  ),  1 =>   array (    0 => 'text',    1 => '/demo/secured/hello/admin',  ),));
    }

    private function get_demoRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/demo/',  ),));
    }

    private function get_demo_helloRouteInfo()
    {
        return array(array (  0 => 'name',), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::helloAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'name',  ),  1 =>   array (    0 => 'text',    1 => '/demo/hello',  ),));
    }

    private function get_demo_contactRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::contactAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/demo/contact',  ),));
    }

    private function get_wdtRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::toolbarAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  1 =>   array (    0 => 'text',    1 => '/_wdt',  ),));
    }

    private function get_profiler_searchRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_profiler/search',  ),));
    }

    private function get_profiler_purgeRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::purgeAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_profiler/purge',  ),));
    }

    private function get_profiler_importRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::importAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_profiler/import',  ),));
    }

    private function get_profiler_exportRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::exportAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '.txt',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/\\.]+?',    3 => 'token',  ),  2 =>   array (    0 => 'text',    1 => '/_profiler/export',  ),));
    }

    private function get_profiler_search_resultsRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchResultsAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/search/results',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  2 =>   array (    0 => 'text',    1 => '/_profiler',  ),));
    }

    private function get_profilerRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::panelAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  1 =>   array (    0 => 'text',    1 => '/_profiler',  ),));
    }

    private function get_configurator_homeRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_configurator/',  ),));
    }

    private function get_configurator_stepRouteInfo()
    {
        return array(array (  0 => 'index',), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'index',  ),  1 =>   array (    0 => 'text',    1 => '/_configurator/step',  ),));
    }

    private function get_configurator_finalRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_configurator/final',  ),));
    }

    private function getneofood_twitterboostrap_default_indexRouteInfo()
    {
        return array(array (  0 => 'name',), array (  '_controller' => 'NeoFood\\TwitterBoostrapBundle\\Controller\\DefaultController::indexAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'name',  ),  1 =>   array (    0 => 'text',    1 => '/hello',  ),));
    }

    private function getdish_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\AdminBundle\\Controller\\DishController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/administrer/dish/new',  ),));
    }

    private function getdish_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\AdminBundle\\Controller\\DishController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/administrer/dish/create',  ),));
    }

    private function getdish_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'NeoFood\\AdminBundle\\Controller\\DishController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/administrer/dish',  ),));
    }

    private function getdish_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'NeoFood\\AdminBundle\\Controller\\DishController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/administrer/dish',  ),));
    }

    private function getdish_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'NeoFood\\AdminBundle\\Controller\\DishController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/administrer/dish',  ),));
    }

    private function getadminRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\AdminBundle\\Controller\\DefaultController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/administrer/',  ),));
    }

    private function getdishRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\AdminBundle\\Controller\\DefaultController::listDishesAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/administrer/liste-des-plats',  ),));
    }

    private function getdish_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'NeoFood\\AdminBundle\\Controller\\DefaultController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/administrer',  ),));
    }

    private function gettbleRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\TbleController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/tble/',  ),));
    }

    private function gettble_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\TbleController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/tble',  ),));
    }

    private function gettble_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\TbleController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/tble/new',  ),));
    }

    private function gettble_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\TbleController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/tble/create',  ),));
    }

    private function gettble_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\TbleController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/tble',  ),));
    }

    private function gettble_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\TbleController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/tble',  ),));
    }

    private function gettble_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\TbleController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/tble',  ),));
    }

    private function getrestaurantRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\RestaurantController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/restaurant/',  ),));
    }

    private function getrestaurant_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\RestaurantController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/restaurant',  ),));
    }

    private function getrestaurant_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\RestaurantController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/restaurant/new',  ),));
    }

    private function getrestaurant_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\RestaurantController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/restaurant/create',  ),));
    }

    private function getrestaurant_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\RestaurantController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/restaurant',  ),));
    }

    private function getrestaurant_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\RestaurantController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/restaurant',  ),));
    }

    private function getrestaurant_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\RestaurantController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/restaurant',  ),));
    }

    private function getfos_user_registration_register_customRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\RestoRegistrationController::registerAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/register',  ),));
    }

    private function gethomeRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/',  ),));
    }

    private function getwatchTablesRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::watchTablesAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/serveur',  ),));
    }

    private function gettableRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::tableAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/table',  ),));
    }

    private function getviewDetailRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::viewDetailsAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/detailDish',  ),));
    }

    private function getaddToCartRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::addToCartAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/ajouter-au-panier',  ),));
    }

    private function getsubToCartRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::subToCartAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/retirer-du-panier',  ),));
    }

    private function getshowCartRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::showCartAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/visualiser-panier',  ),));
    }

    private function getsubmitCartRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::submitCartAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/paiement',  ),));
    }

    private function getvalidPaiementCartRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::validPaiementCartAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/paiement-valide',  ),));
    }

    private function getcookRouteInfo()
    {
        return array(array (  0 => 'commandeId',), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::cookAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'commandeId',  ),  1 =>   array (    0 => 'text',    1 => '/preparer-un-plat',  ),));
    }

    private function getcookReadyRouteInfo()
    {
        return array(array (  0 => 'commandeId',), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::cookReadyAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'commandeId',  ),  1 =>   array (    0 => 'text',    1 => '/plat-pret',  ),));
    }

    private function getmanageRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::manageAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/manage',  ),));
    }

    private function getassignWaiterRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::assignWaiterAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/assignWaiter',  ),));
    }

    private function getuserRouteInfo()
    {
        return array(array (  0 => 'type',  1 => 'id',), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::userAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'type',  ),  2 =>   array (    0 => 'text',    1 => '/user',  ),));
    }

    private function getresetRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\DefaultController::resetAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/reset',  ),));
    }

    private function getwaiterRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\WaiterController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/waiter/',  ),));
    }

    private function getwaiter_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\WaiterController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/waiter',  ),));
    }

    private function getwaiter_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\WaiterController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/waiter/new',  ),));
    }

    private function getwaiter_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\WaiterController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/waiter/create',  ),));
    }

    private function getwaiter_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\WaiterController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/waiter',  ),));
    }

    private function getwaiter_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\WaiterController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/waiter',  ),));
    }

    private function getwaiter_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'NeoFood\\RestoBundle\\Controller\\WaiterController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/waiter',  ),));
    }

    private function getfos_user_security_loginRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/login',  ),));
    }

    private function getfos_user_security_checkRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/login_check',  ),));
    }

    private function getfos_user_security_logoutRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/logout',  ),));
    }

    private function getfos_user_profile_showRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/profile/',  ),));
    }

    private function getfos_user_profile_editRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/profile/edit',  ),));
    }

    private function getfos_user_registration_registerRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/register/',  ),));
    }

    private function getfos_user_registration_check_emailRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/register/check-email',  ),));
    }

    private function getfos_user_registration_confirmRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  1 =>   array (    0 => 'text',    1 => '/register/confirm',  ),));
    }

    private function getfos_user_registration_confirmedRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/register/confirmed',  ),));
    }

    private function getfos_user_resetting_requestRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/resetting/request',  ),));
    }

    private function getfos_user_resetting_send_emailRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',), array (  '_method' => 'POST',), array (  0 =>   array (    0 => 'text',    1 => '/resetting/send-email',  ),));
    }

    private function getfos_user_resetting_check_emailRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/resetting/check-email',  ),));
    }

    private function getfos_user_resetting_resetRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',), array (  '_method' => 'GET|POST',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  1 =>   array (    0 => 'text',    1 => '/resetting/reset',  ),));
    }

    private function getfos_user_change_passwordRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',), array (  '_method' => 'GET|POST',), array (  0 =>   array (    0 => 'text',    1 => '/profile/change-password',  ),));
    }

    private function getorderly_paypalipn_twignotificationemail_indexRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Orderly\\PayPalIpnBundle\\Controller\\TwigNotificationEmailController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/ipn/ipn-twig-email-notification',  ),));
    }
}
